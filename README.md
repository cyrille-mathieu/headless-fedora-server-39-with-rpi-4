# Installing Fedora Server 39 headless on Raspberry Pi 4B

This project is forked from my previous headless install from Fedora 38 Server. When Fedora 39 released, I followed the update instructions, aware of any bugs [present](https://discussion.fedoraproject.org/t/talk-f38-to-f39-dnf-system-upgrade-can-fail-on-raspberry-pi/92815#!) with the upgrade process. Spoiler: the upgrade still bricked my existing install and my RPi didn't want to boot afterwards. This project serves as an updated version of my previous headless install, now updated for Fedora 39 Server and with less Dan Walsh weeping.

## Table of Contents

- [1.0 Prerequisites](#prerequisites)
- [2.0 Preparing Fedora 39 Server for headless install](#20-preparing-fedora-39-server-for-headless-install)
    - [2.1 Acquiring the image](#21-acquiring-the-image)
    - [2.2 Mounting the image](#22-mounting-the-image)
    - [2.3 Modifying the image](#23-modifying-the-image)
    - [2.4 Cleaning up the working environment](#24-cleaning-up-the-working-environment)
    - [2.5 Installing the image](#25-installing-the-image)

## 1.0 Prerequisites

Prerequisites used in this project:

- Raspberry Pi 4B (4GB)
- 32GB SDXC card
- Wired internet connectivity for the RPi
- Fedora 39 Server aarch64 raw image
- As a host system: a working installation of Fedora 38 Workstation (F39 Workstation not tested) with KVM & QEMU
- Packages:
  - arm-image-installer
  - setroubleshoot
  - qemu-user-static

## 2.0 Preparing Fedora 39 Server for headless install

The high-level overview of the steps in this chapter are as follows:
1. Acquire image
2. Decompress image
3. Mount the image
4. Modify image
5. Compress image
6. Write image to disk

### 2.1 Acquiring the image

The RPi 4B has a [BCM2711](https://www.raspberrypi.com/documentation/computers/processors.html#bcm2711) SoC, the CPU is an [ARM Cortex-A72](https://en.wikipedia.org/wiki/ARM_Cortex-A72) which implements the [ARMv8](https://en.wikipedia.org/wiki/ARM_architecture_family#Armv8) core and uses ARM's AArch64 architecture. This means we need to download the Fedora 39 Server raw image for aarch64. Download it with your favourite CLI tool from the the following location:

> `wget https://download.fedoraproject.org/pub/fedora/linux/releases/39/Server/aarch64/images/Fedora-Server-39-1.5.aarch64.raw.xz`

**Note:** Please keep in mind that the location and image may have changed from the time this was written. If so, have a look for yourself [here](https://fedoraproject.org/server/) where you can download the OS image.

I recommend to make a copy of the downloaded image to prevent having to downlad the image again in case we mess up something:

> `cp ./Fedora-Server-39-1.5.aarch64.raw.xz ./Fedora-Server-39-1.5.aarch64.raw.xz.bak`

Before we can work with the image, we have to decompress it like so:

> `xz -d ./Fedora-Server-39-1.5.aarch64.raw.xz`

### 2.2 Mounting the image

Before we can boot Fedora 39 Server in a headless environment, we need to modify it. More specifically, we need to modify the partition with id `8e` from our raw image. However, before we can modify it, we need to mount the image locally.

We cannot mount an LVM partition directly, but `kpartx` helps us out here in creating a device mapping from the partition table in the `.raw` image:

> `sudo kpartx -av ./Fedora-Server-39-1.5.aarch64.raw`

Have a look with:

> `ls -l /dev/mapper/`

Normally you should see 3 loop devices and a Fedora root partition. For the Fedora 39 image, that root partition should be named something like this: `fedora-root`.

Create a directory and mount the Fedora root partition:

> `sudo mkdir /mnt/fedora` <br>
> `sudo mount /dev/mapper/fedora-root /mnt/fedora`

To check that the Fedora root partition was mounted correctly, use `lsblk` and have a look at the mountpoint. If it is not mounted in the correct location, unmount it with:

> `sudo umount /mnt/fedora`

And mount it correctly to your location of choice.

In my case the terminal output looked like this:

```text
NAME                                          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
loop0                                           7:0    0     7G  0 loop  
├─loop0p1                                     253:1    0   600M  0 part  
├─loop0p2                                     253:2    0     1G  0 part  
└─loop0p3                                     253:3    0   5.4G  0 part  
  └─fedora-root                               253:4    0   5.4G  0 lvm   /mnt/fedora
```

### 2.3 Modifying the image

Make sure that the `systemd-binfmt` service is running. If not, start it, we only need to start it for this session:

> `sudo systemctl start systemd-binfmt`

`chroot` to the mounted partition and open a Bash shell in it:

> `sudo chroot /mnt/fedora /bin/bash`

Verify the environment's processor type is `aarch64`:

> `uname -m`

We will create a user with root permissions, this will also be the user we will use to manage the system later on. I went with the very creative username `pi`, feel free to choose the username you desire.

> `groupadd pi` <br>
> `useradd -g pi -G wheel -m -u 1000 pi`

#### 2.3.1 Setting the password

Our freshly created user doesn't have a password yet, which may prove difficult when we try to login to our headless Pi later on. We will add a SSH key later, and first set a password: 

`passwd pi`

Executing the `passwd` command above presented me with the following errors:

> `qemu-aarch64-static: qemu_mprotect__osdep: mprotect failed: Permission denied` <br>
> `qemu-aarch64-static: mprotect of jit buffer: Permission denied`

SELinux is at work here, doing it's job. Let's [not disable it](https://stopdisablingselinux.com/), open a new terminal on our host and let's troubleshoot this issue:

> `journalctl -t setroubleshoot`

You should see the following:

```text
Nov 30 23:15:43 fedora setroubleshoot[4989]: SELinux is preventing passwd from using the execmem access on a process. For complete SELinux messages run: sealert -l 33073ff5-f092-4d46-8443-96a702d02973
Nov 30 23:15:44 fedora setroubleshoot[4989]: SELinux is preventing passwd from using the execmem access on a process.

                                             *****  Plugin allow_execmem (91.4 confidence) suggests   *********************

                                             If this issue occurred during normal system operation.
                                             Then this alert could be a serious issue and your system could be compromised.
                                             Do
                                             contact your security administrator and report this issue

                                             *****  Plugin catchall (9.59 confidence) suggests   **************************

                                             If you believe that passwd should be allowed execmem access on processes labeled passwd_t by default.
                                             Then you should report this as a bug.
                                             You can generate a local policy module to allow this access.
                                             Do
                                             allow this access for now by executing:
                                             # ausearch -c 'passwd' --raw | audit2allow -M my-passwd
                                             # semodule -X 300 -i my-passwd.pp
```
setroubleshoot nicely presents us what is prohibiting our `passwd` command from working in our `chroot` environment! We can execute the second line from our output to view the issue in more detail, but for now we can follow what setroubleshoot tells us to resolve the problem:

> `sudo ausearch -c 'passwd' --raw | audit2allow -M my-passwd` <br>
> `sudo semodule -X 300 -i my-passwd.pp`

Let's try the `passwd pi` command again in our `chroot` environment. A new error presents itself: `passwd: Authentication token manipulation error`. The error seems to be the permissions on the `/etc/shadow` file on our host machine. Exit the `chroot` environment and let's have a look at the `/etc/shadow` file with:

> `ls -l /etc/shadow`

```text
----------. 1 root root 1351 Nov 30 22:33 /etc/shadow
```

The permissions are 000, which seems to be a problem when we execute the `passwd` command from our `chroot` environment. I don't know the specifics of this, but looking at [this](https://unix.stackexchange.com/questions/549464/etc-shadow-permissions-security-best-practice-000-vs-600-vs-640) thread, let's try setting the permissions to 600 instead.

> `sudo chmod 600 /etc/shadow`

Now we try again in our `chroot` environment:

> `passwd pi`

And the following line should be printed: `passwd: all authentication tokens updated successfully.`

If desired, you can also do the same for the `root` user.

#### 2.3.2 Setting SSH keys

SSH keys are a useful and secure tool for logging in to (remote) machines, the following command creates the `.ssh` directory and `authorized_keys` file and sets the correct permissions for both:

> `mkdir /home/pi/.ssh && chmod 700 $_ && touch $_/authorized_keys && chmod 600 $_`

Set the correct ownership:

> `chown -R pi:pi /home/pi/.ssh/`

Add your **public** (`.pub`) SSH key to the freshly created `/home/pi/.ssh/authorized_keys` file.

#### 2.3.3 Unlinking the initial setup services

Upon the first boot of the RPi, some `systemd` targets will look for specific services. Disable these:

> `unlink /etc/systemd/system/multi-user.target.wants/initial-setup.service` <br>
> `unlink /etc/systemd/system/graphical.target.wants/initial-setup.service`

### 2.4 Cleaning up the working environment

Exit the `chroot` environment and unmount the Fedora root partition:

> `sudo umount /mnt/fedora && sudo rmdir $_`

Scan the physical volumes first:

> `sudo pvscan`

Following that, deactivate the volume group:

> `sudo vgchange -a n fedora`

And finally, unmap the disk image:

> `sudo kpartx -d ./Fedora-Server-39-1.5.aarch64.raw`

Check with `lsblk` to make sure all partitions were removed.

### 2.5 Installing the image

Compress the disk image, read the `xz` man pages if you need more information for the parameters. This may take a couple of minutes, depending on the number of threads your CPU has:

> `xz -zk -T0 ./Fedora-Server-39-1.5.aarch64.raw`

> **Note:** I recommend including the `-k` parameter in the command above, this *keeps* the original `.raw` disk image. This can prove to be useful if you need to make some (quick) changes to the image. Alternatively, store a copy of the compressed image for your own, later reference.

#### 2.5.1 Writing the image

Connect your SD(XC) card to your computer and find out where it is mounted with `lsblk`.

Write the prepared image to your SD card:

> `sudo arm-image-installer --image=./Fedora-Server-39-1.5.aarch64.raw.xz --media=/dev/sdX --target=rpi4 --resizefs -y`

Once this is done (it can take a couple minutes), safely disconnect the SD card from your machine, insert the micro SD card into your RPi and make sure your RPi has a wired internet connection and can receive an IP address via DHCP.

Lastly, power on your RPi.

#### 2.5.2 Connecting to your headless Raspberry Pi

If all goes well, your RPi should now be booting Fedora 39 Server. The `sshd.service` should come online automagically and you can now SSH into your RPi.

Obviously, you first need to know the IP address of your RPi, which you can obtain by:

1) Checking the connected devices via your router
2) Viewing the active leases if you have configured a separate DHCP server on your network
3) Scanning your ***local*** network with a tool like nmap: `nmap -Pn 192.168.0.0/24`

Once you have the IP of your RPi, connect:

`ssh pi@<RPi-IP-address>`